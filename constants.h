#ifndef CONSTANTS_H
#define CONSTANTS_H

const float probability_distribution[4][4] = {{0,    0,     0.1f, 0.1f},
                                              {0.2f, 0,     0,    0.2f},
                                              {0,    0,     0.3f, 0},
                                              {0,    0.05f, 0,    0.05f}};
const int distribution_x_values[] = {1, 2, 3, 4};
const int distribution_y_values[] = {1, 2, 3, 4};

const int x_values_tab_size =
        sizeof(distribution_x_values) / sizeof(*distribution_x_values);
const int y_values_tab_size =
        sizeof(distribution_y_values) / sizeof(*distribution_y_values);

#endif
