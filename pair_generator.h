#ifndef PAIR_GENERATOR_H
#define PAIR_GENERATOR_H

#include "pair.h"

pair generate_pair(int **statistics_table);

int **create_statistics_table();

void print_statistics(int **statistics_table);

void free_statistics_table(int **statistics_table);

#endif
