#include <stdlib.h>
#include "rand_helper.h"


float rand_float_from_range(float begin, float end) {
    float random_value = (float) rand() / RAND_MAX;
    return random_value * (end - begin) + begin;
}

int rand_integer_from_range(int begin, int end) {
    return rand() % (end - begin + 1) + begin;
}
