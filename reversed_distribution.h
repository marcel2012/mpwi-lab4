#ifndef REVERSED_DISTRIBUTION_H
#define REVERSED_DISTRIBUTION_H

int random_value_by_reversed_distribution(const float probability_distribution[],
                                          const int probability_distribution_tab_size);

#endif
