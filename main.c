#include "pair_generator.h"


int main() {
    pair random_pair;
    int repetitions = 10000;

    int **statistics_table = create_statistics_table();

    while (repetitions--) {
        random_pair = generate_pair(statistics_table);
        print_pair(random_pair);
    }

    print_statistics(statistics_table);

    free_statistics_table(statistics_table);
    return 0;
}
