#include "reversed_distribution.h"
#include "rand_helper.h"


int random_value_by_reversed_distribution(const float probability_distribution[],
                                          const int probability_distribution_tab_size) {
    float probabilities_sum = 0;
    for (int i = 0; i < probability_distribution_tab_size; i++) {
        probabilities_sum += probability_distribution[i];
    }

    float random_value = rand_float_from_range(0, probabilities_sum);

    float cumulative_distribution = 0;
    for (int i = 0; i < probability_distribution_tab_size - 1; i++) {
        cumulative_distribution += probability_distribution[i];

        if (cumulative_distribution >= random_value) {
            return i;
        }
    }
    return probability_distribution_tab_size - 1;
}
