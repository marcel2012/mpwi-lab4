#ifndef RAND_HELPER_H
#define RAND_HELPER_H

float rand_float_from_range(float begin, float end);

int rand_integer_from_range(int begin, int end);

#endif
