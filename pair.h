#ifndef PAIR_H
#define PAIR_H

typedef struct pair pair;
struct pair {
    int x;
    int y;
};

void print_pair(pair p);

#endif
