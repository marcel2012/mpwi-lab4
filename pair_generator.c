#include <stdlib.h>
#include <stdio.h>
#include "pair_generator.h"
#include "rand_helper.h"
#include "constants.h"
#include "reversed_distribution.h"


void add_to_statistics(int **statistics_table, const int x_index, const int y_index) {
    statistics_table[y_index][x_index]++;
}

pair generate_pair(int **statistics_table) {
    int x_value_index = rand_integer_from_range(0, x_values_tab_size - 1);
    int x_value = distribution_x_values[x_value_index];

    int y_value_index = random_value_by_reversed_distribution(probability_distribution[x_value_index],
                                                              y_values_tab_size);
    int y_value = distribution_y_values[y_value_index];
    pair generated_pair = {x_value, y_value};

    if (statistics_table != NULL) {
        add_to_statistics(statistics_table, x_value_index, y_value_index);
    }
    return generated_pair;
}

int **create_statistics_table() {
    int **table = (int **) malloc(y_values_tab_size * sizeof(int *));
    int *values = (int *) calloc(x_values_tab_size * y_values_tab_size, sizeof(int));
    for (int y_index = 0; y_index < y_values_tab_size; y_index++) {
        table[y_index] = values + x_values_tab_size * y_index;
    }
    return table;
}

void print_statistics(int **statistics_table) {
    int count = 0;
    for (int x_index = 0; x_index < x_values_tab_size; x_index++) {
        for (int y_index = 0; y_index < y_values_tab_size; y_index++) {
            count += statistics_table[y_index][x_index];
        }
    }
    for (int x_index = 0; x_index < x_values_tab_size; x_index++) {
        for (int y_index = 0; y_index < y_values_tab_size; y_index++) {
            printf("%f ", (float) statistics_table[y_index][x_index] / count);
        }
        printf("\n");
    }
}

void free_statistics_table(int **statistics_table) {
    free(statistics_table[0]);
    free(statistics_table);
}
